package service.Impl;

import constant.SystemConstant;
import service.MySqlHandler;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.List;

/**
 * @author Aimony
 * @date 2023/10/10 19:07
 * @describe 将数据插入到数据库中
 */
public class MySqlHandlerImpl implements MySqlHandler {
    @Override
    public boolean addData(List<String> tags) {
        try {
            final Class<?> clazz = Class.forName("com.mysql.cj.jdbc.Driver");
            final Constructor<?> constructor = clazz.getDeclaredConstructor();
            final Driver driver =(Driver)constructor.newInstance();

            String url = SystemConstant.URL;
            String username = SystemConstant.USERNAME;
            String password = SystemConstant.PASSWORD;

            System.out.println("--------------");

            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(url, username, password);


            String insertQuery = "INSERT INTO sina_data (tag) VALUES (?)";
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);

            for (String tag : tags) {
                preparedStatement.setString(1, tag);
                preparedStatement.executeUpdate();
            }

            System.out.println("插入数据库完成！");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException | InvocationTargetException | NoSuchMethodException | InstantiationException |
                 IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return false;
    }
}
