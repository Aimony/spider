import domain.SinaSpider;

import java.io.IOException;

public class DoSpider {
    public static void main(String[] args) throws IOException {
        SinaSpider sinaSpider = new SinaSpider();
        sinaSpider.start();
    }
}
