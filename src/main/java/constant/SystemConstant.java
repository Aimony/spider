package constant;

/**
 * @author Aimony
 * @date 2023/10/10 20:12
 * @describe
 */
public final class SystemConstant {

    /**
     * 目标url
     */
    public static final String DESTINATION_URL = "https://www.sina.com.cn/";
    public static final Integer TIMEOUT = 300000;

    /**
     * 数据库相关配置
     */
    public static final String URL = "jdbc:mysql://localhost:3306/reptile";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "123456";

}
