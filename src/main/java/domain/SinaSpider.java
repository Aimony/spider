package domain;

import constant.SystemConstant;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class SinaSpider extends BaseSpider {
    @Override
    String open() {
        return SystemConstant.DESTINATION_URL;
    }
    @Override
    List<String> parseData(Document document) {
        Elements elementsByClass = document.getElementsByClass("nav-mod-1");
        List<String> tagList = new ArrayList<>();
        for (Element el : elementsByClass) {
            Elements aTag = el.select("li a");
            for (Element aTagText : aTag) {
                String text = aTagText.text();
                System.out.println(text);
                tagList.add(text);
            }
        }
        return tagList;
    }


}
