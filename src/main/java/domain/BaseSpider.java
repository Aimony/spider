package domain;

import constant.SystemConstant;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import service.Impl.MySqlHandlerImpl;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public abstract class BaseSpider extends Thread {
    abstract String open();

    abstract List<String> parseData(Document document);

    public void run() {
        //打开网站
        String url = this.open();
        //使用jourp 请求数据
        Document parse = null;
        try {
            parse = Jsoup.parse(new URL(url), SystemConstant.TIMEOUT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //解析数据
        List<String> tags = this.parseData(parse);
        //写入到数据
        this.intoDb(tags);
    }

    private void intoDb(List<String> tags) {
        MySqlHandlerImpl mysqlHandler = new MySqlHandlerImpl();
        mysqlHandler.addData(tags);
    }
}
